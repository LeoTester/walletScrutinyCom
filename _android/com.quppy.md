---
title: "Quppy Wallet - bitcoin, crypto and euro payments"
altTitle: 

users: 50000
appId: com.quppy
launchDate: 
latestUpdate: 2020-11-13
apkVersionName: "1.0.38"
stars: 4.7
ratings: 1934
reviews: 912
size: 15M
website: 
repository: 
issue: 
icon: com.quppy.png
bugbounty: 
verdict: wip # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-11-16
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.quppy/
---


This page was created by a script from the **appId** "com.quppy" and public
information found
[here](https://play.google.com/store/apps/details?id=com.quppy).

Probably an engineer will soon have a deeper look at this app.

So far we are not even sure it is a wallet ... Please check back later.