---
title: "arzjoo"
altTitle: 

users: 10000
appId: com.arzjoo
launchDate: 
latestUpdate: 2020-11-14
apkVersionName: "1.3.2"
stars: 4.1
ratings: 854
reviews: 347
size: Varies with device
website: 
repository: 
issue: 
icon: com.arzjoo.png
bugbounty: 
verdict: wip # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-11-16
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.arzjoo/
---


This page was created by a script from the **appId** "com.arzjoo" and public
information found
[here](https://play.google.com/store/apps/details?id=com.arzjoo).

Probably an engineer will soon have a deeper look at this app.

So far we are not even sure it is a wallet ... Please check back later.