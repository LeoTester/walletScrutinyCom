---
title: "Kraken Futures: Bitcoin & Crypto Futures Trading"
altTitle: 

users: 10000
appId: com.krakenfutures
launchDate: 
latestUpdate: 2020-11-11
apkVersionName: "5.22.1"
stars: 3.5
ratings: 52
reviews: 20
size: 12M
website: https://futures.kraken.com/
repository: 
issue: 
icon: com.krakenfutures.png
bugbounty: 
verdict: custodial # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-06-20
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.krakenfutures/
  - /posts/com.krakenfutures/
---


This is the interface for an exchange and nothing in the description hints at
non-custodial parts to it.
