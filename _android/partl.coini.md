---
title: "Coini — Bitcoin / Cryptocurrencies"
altTitle: 

users: 1000
appId: partl.coini
launchDate: 
latestUpdate: 2020-11-16
apkVersionName: "1.8.29"
stars: 4.5
ratings: 126
reviews: 68
size: 94M
website: 
repository: 
issue: 
icon: partl.coini.png
bugbounty: 
verdict: wip # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-11-16
reviewStale: false
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /partl.coini/
---


This page was created by a script from the **appId** "partl.coini" and public
information found
[here](https://play.google.com/store/apps/details?id=partl.coini).

Probably an engineer will soon have a deeper look at this app.

So far we are not even sure it is a wallet ... Please check back later.