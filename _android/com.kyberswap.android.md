---
title: "KyberSwap: Buy, Trade, Transfer Cryptocurrencies"
altTitle: 

users: 10000
appId: com.kyberswap.android
launchDate: 
latestUpdate: 2020-10-16
apkVersionName: "1.1.33"
stars: 4.2
ratings: 1701
reviews: 820
size: 19M
website: 
repository: 
issue: 
icon: com.kyberswap.android.png
bugbounty: 
verdict: wip # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-11-16
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.kyberswap.android/
---


This page was created by a script from the **appId** "com.kyberswap.android" and public
information found
[here](https://play.google.com/store/apps/details?id=com.kyberswap.android).

Probably an engineer will soon have a deeper look at this app.

So far we are not even sure it is a wallet ... Please check back later.