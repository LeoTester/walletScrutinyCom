---
title: "Bitcoin & Crypto Exchange - BitBay"
altTitle: 

users: 100000
appId: net.bitbay.bitcoin
launchDate: 
latestUpdate: 2020-11-05
apkVersionName: "1.1.15"
stars: 4.0
ratings: 637
reviews: 327
size: 16M
website: 
repository: 
issue: 
icon: net.bitbay.bitcoin.png
bugbounty: 
verdict: wip # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-11-16
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /net.bitbay.bitcoin/
---


This page was created by a script from the **appId** "net.bitbay.bitcoin" and public
information found
[here](https://play.google.com/store/apps/details?id=net.bitbay.bitcoin).

Probably an engineer will soon have a deeper look at this app.

So far we are not even sure it is a wallet ... Please check back later.