---
title: "CoolBitX Crypto"
altTitle: 

users: 10000
appId: com.coolbitx.cwsapp
launchDate: 
latestUpdate: 2020-11-13
apkVersionName: "2.7.2"
stars: 4.6
ratings: 472
reviews: 225
size: 66M
website: 
repository: 
issue: 
icon: com.coolbitx.cwsapp.png
bugbounty: 
verdict: wip # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-11-16
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.coolbitx.cwsapp/
---


This page was created by a script from the **appId** "com.coolbitx.cwsapp" and public
information found
[here](https://play.google.com/store/apps/details?id=com.coolbitx.cwsapp).

Probably an engineer will soon have a deeper look at this app.

So far we are not even sure it is a wallet ... Please check back later.