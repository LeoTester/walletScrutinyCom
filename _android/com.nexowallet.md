---
title: "Nexo - Crypto Banking Account"
altTitle: 

users: 100000
appId: com.nexowallet
launchDate: 
latestUpdate: 2020-10-28
apkVersionName: "1.0.59"
stars: 4.2
ratings: 4252
reviews: 1946
size: 48M
website: 
repository: 
issue: 
icon: com.nexowallet.png
bugbounty: 
verdict: wip # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-11-16
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.nexowallet/
---


This page was created by a script from the **appId** "com.nexowallet" and public
information found
[here](https://play.google.com/store/apps/details?id=com.nexowallet).

Probably an engineer will soon have a deeper look at this app.

So far we are not even sure it is a wallet ... Please check back later.