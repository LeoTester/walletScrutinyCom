---
title: "SafePal - Crypto wallet BTC ETH LTC BNB Tron EOS"
altTitle: 

users: 5000
appId: io.safepal.wallet
launchDate: 
latestUpdate: 2020-11-06
apkVersionName: "2.3.0"
stars: 4.5
ratings: 121
reviews: 76
size: 26M
website: 
repository: 
issue: 
icon: io.safepal.wallet.png
bugbounty: 
verdict: wip # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-11-16
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /io.safepal.wallet/
---


This page was created by a script from the **appId** "io.safepal.wallet" and public
information found
[here](https://play.google.com/store/apps/details?id=io.safepal.wallet).

Probably an engineer will soon have a deeper look at this app.

So far we are not even sure it is a wallet ... Please check back later.