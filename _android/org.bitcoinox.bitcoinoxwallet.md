---
title: "Bitcoin OX Wallet — Exchange Wallet for Crypto"
altTitle: 

users: 1000
appId: org.bitcoinox.bitcoinoxwallet
launchDate: 
latestUpdate: 2020-05-19
apkVersionName: "2.2.26"
stars: 4.8
ratings: 18
reviews: 14
size: Varies with device
website: https://bitcoinox.com/
repository: 
issue: 
icon: org.bitcoinox.bitcoinoxwallet.png
bugbounty: 
verdict: nosource # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-09-27
reviewStale: false
signer: 
reviewArchive:


providerTwitter: bitcoin_ox
providerLinkedIn: 
providerFacebook: bitcoinoxwallet
providerReddit: 

redirect_from:
  - /org.bitcoinox.bitcoinoxwallet/
  - /posts/org.bitcoinox.bitcoinoxwallet/
---


> Safety
> 
> • Due to Bitcoin OX, only you are in control of your Private Keys and manage
>   your Digital Assets
> 
> • No copies on our or public servers
> 
> • Compatible with BIP39 mnemonic code for generating deterministic keys

sounds like a non-custodial wallet but as there is no public source code linked
on their Google Play description or their website. This wallet is **not verifiable**.