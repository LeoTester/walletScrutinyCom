---
title: "Moonlet"
altTitle: 

users: 5000
appId: com.moonlet
launchDate: 
latestUpdate: 2020-11-08
apkVersionName: "1.4.16"
stars: 4.2
ratings: 103
reviews: 73
size: 8.8M
website: 
repository: 
issue: 
icon: com.moonlet.png
bugbounty: 
verdict: wip # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-11-16
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.moonlet/
---


This page was created by a script from the **appId** "com.moonlet" and public
information found
[here](https://play.google.com/store/apps/details?id=com.moonlet).

Probably an engineer will soon have a deeper look at this app.

So far we are not even sure it is a wallet ... Please check back later.