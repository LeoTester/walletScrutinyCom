---
title: "Ledger Live"
altTitle: 

users: 100000
appId: com.ledger.live
launchDate: 
latestUpdate: 2020-10-27
apkVersionName: "2.15.0"
stars: 3.9
ratings: 2104
reviews: 1188
size: Varies with device
website: 
repository: 
issue: 
icon: com.ledger.live.png
bugbounty: 
verdict: wip # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-11-16
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.ledger.live/
---


This page was created by a script from the **appId** "com.ledger.live" and public
information found
[here](https://play.google.com/store/apps/details?id=com.ledger.live).

Probably an engineer will soon have a deeper look at this app.

So far we are not even sure it is a wallet ... Please check back later.