---
title: "Good Crypto: one trading app for all exchanges"
altTitle: 

users: 10000
appId: app.goodcrypto
launchDate: 
latestUpdate: 2020-11-05
apkVersionName: "1.5"
stars: 4.6
ratings: 104
reviews: 60
size: 17M
website: 
repository: 
issue: 
icon: app.goodcrypto.png
bugbounty: 
verdict: wip # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-11-16
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /app.goodcrypto/
---


This page was created by a script from the **appId** "app.goodcrypto" and public
information found
[here](https://play.google.com/store/apps/details?id=app.goodcrypto).

Probably an engineer will soon have a deeper look at this app.

So far we are not even sure it is a wallet ... Please check back later.