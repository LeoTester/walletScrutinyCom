---
title: "TronWallet: Bitcoin Blockchain Wallet"
altTitle: 

users: 1000000
appId: com.tronwallet2
launchDate: 
latestUpdate: 2020-07-23
apkVersionName: "3.4.5"
stars: 3.8
ratings: 7828
reviews: 4303
size: Varies with device
website: 
repository: 
issue: 
icon: com.tronwallet2.png
bugbounty: 
verdict: wip # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-11-16
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.tronwallet2/
---


This page was created by a script from the **appId** "com.tronwallet2" and public
information found
[here](https://play.google.com/store/apps/details?id=com.tronwallet2).

Probably an engineer will soon have a deeper look at this app.

So far we are not even sure it is a wallet ... Please check back later.