---
title: "Swipe Wallet"
altTitle: 

users: 50000
appId: com.swipe.wallet
launchDate: 
latestUpdate: 2020-11-13
apkVersionName: "1.518"
stars: 3.5
ratings: 1504
reviews: 919
size: 53M
website: 
repository: 
issue: 
icon: com.swipe.wallet.png
bugbounty: 
verdict: wip # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-11-16
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.swipe.wallet/
---


This page was created by a script from the **appId** "com.swipe.wallet" and public
information found
[here](https://play.google.com/store/apps/details?id=com.swipe.wallet).

Probably an engineer will soon have a deeper look at this app.

So far we are not even sure it is a wallet ... Please check back later.