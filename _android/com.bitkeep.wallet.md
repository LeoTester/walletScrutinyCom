---
title: "BitKeep Wallet Pro"
altTitle: 

users: 10000
appId: com.bitkeep.wallet
launchDate: 
latestUpdate: 2020-11-04
apkVersionName: "4.6.1"
stars: 4.8
ratings: 146
reviews: 65
size: 27M
website: 
repository: 
issue: 
icon: com.bitkeep.wallet.png
bugbounty: 
verdict: wip # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-11-16
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.bitkeep.wallet/
---


This page was created by a script from the **appId** "com.bitkeep.wallet" and public
information found
[here](https://play.google.com/store/apps/details?id=com.bitkeep.wallet).

Probably an engineer will soon have a deeper look at this app.

So far we are not even sure it is a wallet ... Please check back later.