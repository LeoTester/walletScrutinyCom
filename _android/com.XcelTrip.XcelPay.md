---
title: "XcelPay - Secure Bitcoin & Ethereum Wallet"
altTitle: 

users: 10000
appId: com.XcelTrip.XcelPay
launchDate: 
latestUpdate: 2020-11-13
apkVersionName: "2.11.3"
stars: 3.9
ratings: 326
reviews: 213
size: 24M
website: 
repository: 
issue: 
icon: com.XcelTrip.XcelPay.jpg
bugbounty: 
verdict: wip # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-11-16
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.XcelTrip.XcelPay/
---


This page was created by a script from the **appId** "com.XcelTrip.XcelPay" and public
information found
[here](https://play.google.com/store/apps/details?id=com.XcelTrip.XcelPay).

Probably an engineer will soon have a deeper look at this app.

So far we are not even sure it is a wallet ... Please check back later.