---
title: "ShapeShift Buy & Trade Bitcoin & Top Crypto Assets"
altTitle: 

users: 100000
appId: com.shapeshift.droid_shapeshift
launchDate: 
latestUpdate: 2020-11-06
apkVersionName: "2.7.1"
stars: 3.3
ratings: 1776
reviews: 1133
size: 51M
website: 
repository: 
issue: 
icon: com.shapeshift.droid_shapeshift.png
bugbounty: 
verdict: wip # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-11-16
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.shapeshift.droid_shapeshift/
---


This page was created by a script from the **appId** "com.shapeshift.droid_shapeshift" and public
information found
[here](https://play.google.com/store/apps/details?id=com.shapeshift.droid_shapeshift).

Probably an engineer will soon have a deeper look at this app.

So far we are not even sure it is a wallet ... Please check back later.