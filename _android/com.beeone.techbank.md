---
title: "TechBank"
altTitle: 

users: 5000
appId: com.beeone.techbank
launchDate: 
latestUpdate: 2020-11-12
apkVersionName: "3.7.9"
stars: 4.6
ratings: 238
reviews: 101
size: 41M
website: 
repository: 
issue: 
icon: com.beeone.techbank.png
bugbounty: 
verdict: wip # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-11-16
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.beeone.techbank/
---


This page was created by a script from the **appId** "com.beeone.techbank" and public
information found
[here](https://play.google.com/store/apps/details?id=com.beeone.techbank).

Probably an engineer will soon have a deeper look at this app.

So far we are not even sure it is a wallet ... Please check back later.