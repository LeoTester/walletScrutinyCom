---
title: "HyperPay Mobile wallet"
altTitle: 

users: 10000
appId: com.legendwd.hyperpayW
launchDate: 
latestUpdate: 2020-11-13
apkVersionName: "4.0.3"
stars: 4.5
ratings: 694
reviews: 558
size: 107M
website: 
repository: 
issue: 
icon: com.legendwd.hyperpayW.png
bugbounty: 
verdict: wip # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-11-16
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.legendwd.hyperpayW/
---


This page was created by a script from the **appId** "com.legendwd.hyperpayW" and public
information found
[here](https://play.google.com/store/apps/details?id=com.legendwd.hyperpayW).

Probably an engineer will soon have a deeper look at this app.

So far we are not even sure it is a wallet ... Please check back later.