---
title: "Algorand Wallet"
altTitle: 

users: 10000
appId: com.algorand.android
launchDate: 
latestUpdate: 2020-10-07
apkVersionName: "4.4.3"
stars: 4.5
ratings: 163
reviews: 96
size: 32M
website: 
repository: 
issue: 
icon: com.algorand.android.png
bugbounty: 
verdict: wip # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-11-16
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.algorand.android/
---


This page was created by a script from the **appId** "com.algorand.android" and public
information found
[here](https://play.google.com/store/apps/details?id=com.algorand.android).

Probably an engineer will soon have a deeper look at this app.

So far we are not even sure it is a wallet ... Please check back later.