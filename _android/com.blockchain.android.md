---
title: "Blockchain"
altTitle: 

users: 10000
appId: com.blockchain.android
launchDate: 
latestUpdate: 2020-11-16
apkVersionName: "1.0.5.6"
stars: 4.4
ratings: 1805
reviews: 358
size: 21M
website: 
repository: 
issue: 
icon: com.blockchain.android.png
bugbounty: 
verdict: wip # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-11-16
reviewStale: false
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.blockchain.android/
---


This page was created by a script from the **appId** "com.blockchain.android" and public
information found
[here](https://play.google.com/store/apps/details?id=com.blockchain.android).

Probably an engineer will soon have a deeper look at this app.

So far we are not even sure it is a wallet ... Please check back later.