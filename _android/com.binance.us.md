---
title: "Binance.US: Buy Bitcoin with USD.  Crypto Wallet"
altTitle: 

users: 100000
appId: com.binance.us
launchDate: 
latestUpdate: 2020-11-14
apkVersionName: "1.3.0"
stars: 3.0
ratings: 650
reviews: 441
size: Varies with device
website: 
repository: 
issue: 
icon: com.binance.us.png
bugbounty: 
verdict: wip # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-11-16
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.binance.us/
---


This page was created by a script from the **appId** "com.binance.us" and public
information found
[here](https://play.google.com/store/apps/details?id=com.binance.us).

Probably an engineer will soon have a deeper look at this app.

So far we are not even sure it is a wallet ... Please check back later.