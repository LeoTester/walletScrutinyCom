---
title: "SOLO Wallet"
altTitle: 

users: 1000
appId: com.sologenicwallet
launchDate: 
latestUpdate: 2020-03-07
apkVersionName: "1.4.1"
stars: 3.6
ratings: 45
reviews: 29
size: 27M
website: 
repository: 
issue: 
icon: com.sologenicwallet.png
bugbounty: 
verdict: nobtc # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-06-20
reviewStale: false
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.sologenicwallet/
  - /posts/com.sologenicwallet/
---


This wallet does not support BTC.
