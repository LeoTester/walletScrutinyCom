---
title: "Bitcoin Wallet Blockchain: Ethereum, Crypto, BTC"
altTitle: 

users: 50000
appId: com.ownrwallet.wallet
launchDate: 
latestUpdate: 2020-11-13
apkVersionName: "1.0.51"
stars: 4.6
ratings: 558
reviews: 488
size: 65M
website: 
repository: 
issue: 
icon: com.ownrwallet.wallet.png
bugbounty: 
verdict: wip # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-11-16
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.ownrwallet.wallet/
---


This page was created by a script from the **appId** "com.ownrwallet.wallet" and public
information found
[here](https://play.google.com/store/apps/details?id=com.ownrwallet.wallet).

Probably an engineer will soon have a deeper look at this app.

So far we are not even sure it is a wallet ... Please check back later.