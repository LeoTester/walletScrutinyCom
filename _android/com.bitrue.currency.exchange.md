---
title: "Bitrue - Cryptocurrency Wallet & Exchange"
altTitle: 

users: 100000
appId: com.bitrue.currency.exchange
launchDate: 
latestUpdate: 2020-11-04
apkVersionName: "4.3.2"
stars: 3.9
ratings: 966
reviews: 623
size: 35M
website: 
repository: 
issue: 
icon: com.bitrue.currency.exchange.png
bugbounty: 
verdict: wip # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-11-16
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.bitrue.currency.exchange/
---


This page was created by a script from the **appId** "com.bitrue.currency.exchange" and public
information found
[here](https://play.google.com/store/apps/details?id=com.bitrue.currency.exchange).

Probably an engineer will soon have a deeper look at this app.

So far we are not even sure it is a wallet ... Please check back later.