---
title: "Bitcoin Wallet & Ethereum Ripple Tron EOS"
altTitle: 

users: 100000
appId: io.atomicwallet
launchDate: 2019-01-30
latestUpdate: 2020-11-12
apkVersionName: "0.67.0"
stars: 4.4
ratings: 17376
reviews: 9263
size: 13M
website: https://atomicwallet.io/
repository: 
issue: 
icon: io.atomicwallet.png
bugbounty: 
verdict: nosource # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-10-12
reviewStale: true
signer: 
reviewArchive:


providerTwitter: atomicwallet
providerLinkedIn: 
providerFacebook: atomicwallet
providerReddit: 

redirect_from:
  - /atomicwallet/
  - /io.atomicwallet/
  - /posts/2019/11/atomicwallet/
  - /posts/io.atomicwallet/
---


Bitcoin Wallet & Ethereum Ripple Tron EOS
is a non-custodial wallet according to their description:

> Atomic Wallet is universal non-custodial app for over 300 cryptocurrencies.

Unfortunately they do not share all sources for the Android app.

Verdict: This wallet is **not verifiable**.
