---
title: "Swap Wallet - BTC, Bitcoin wallet"
altTitle: 

users: 10000
appId: com.swapwalletltd.swap
launchDate: 
latestUpdate: 2020-11-14
apkVersionName: "Varies with device"
stars: 5.0
ratings: 890
reviews: 679
size: Varies with device
website: 
repository: 
issue: 
icon: com.swapwalletltd.swap.png
bugbounty: 
verdict: wip # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-11-16
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.swapwalletltd.swap/
---


This page was created by a script from the **appId** "com.swapwalletltd.swap" and public
information found
[here](https://play.google.com/store/apps/details?id=com.swapwalletltd.swap).

Probably an engineer will soon have a deeper look at this app.

So far we are not even sure it is a wallet ... Please check back later.