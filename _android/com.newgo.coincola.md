---
title: "CoinCola - Buy Bitcoin & more"
altTitle: 

users: 10000
appId: com.newgo.coincola
launchDate: 
latestUpdate: 2020-09-09
apkVersionName: "4.6.1"
stars: 3.1
ratings: 538
reviews: 245
size: 31M
website: 
repository: 
issue: 
icon: com.newgo.coincola.png
bugbounty: 
verdict: wip # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-11-16
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.newgo.coincola/
---


This page was created by a script from the **appId** "com.newgo.coincola" and public
information found
[here](https://play.google.com/store/apps/details?id=com.newgo.coincola).

Probably an engineer will soon have a deeper look at this app.

So far we are not even sure it is a wallet ... Please check back later.