---
title: "LiteBit - Buy & sell Bitcoin"
altTitle: 

users: 10000
appId: com.flutter.litebit
launchDate: 
latestUpdate: 2020-11-16
apkVersionName: "2.11.0"
stars: 3.3
ratings: 179
reviews: 115
size: 78M
website: 
repository: 
issue: 
icon: com.flutter.litebit.png
bugbounty: 
verdict: wip # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-11-16
reviewStale: false
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.flutter.litebit/
---


This page was created by a script from the **appId** "com.flutter.litebit" and public
information found
[here](https://play.google.com/store/apps/details?id=com.flutter.litebit).

Probably an engineer will soon have a deeper look at this app.

So far we are not even sure it is a wallet ... Please check back later.