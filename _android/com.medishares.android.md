---
title: "MathWallet: Bitcoin,Ethereum,EOS,Polkadot,Cosmos"
altTitle: 

users: 10000
appId: com.medishares.android
launchDate: 
latestUpdate: 2020-11-08
apkVersionName: "3.8.0"
stars: 4.1
ratings: 637
reviews: 337
size: 49M
website: 
repository: 
issue: 
icon: com.medishares.android.jpg
bugbounty: 
verdict: wip # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-11-16
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.medishares.android/
---


This page was created by a script from the **appId** "com.medishares.android" and public
information found
[here](https://play.google.com/store/apps/details?id=com.medishares.android).

Probably an engineer will soon have a deeper look at this app.

So far we are not even sure it is a wallet ... Please check back later.