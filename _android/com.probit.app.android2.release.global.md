---
title: "ProBit Global: Buy & Sell Bitcoin. Crypto Exchange"
altTitle: 

users: 50000
appId: com.probit.app.android2.release.global
launchDate: 
latestUpdate: 2020-11-06
apkVersionName: "1.27.1"
stars: 4.0
ratings: 2589
reviews: 1749
size: 17M
website: 
repository: 
issue: 
icon: com.probit.app.android2.release.global.png
bugbounty: 
verdict: wip # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-11-16
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.probit.app.android2.release.global/
---


This page was created by a script from the **appId** "com.probit.app.android2.release.global" and public
information found
[here](https://play.google.com/store/apps/details?id=com.probit.app.android2.release.global).

Probably an engineer will soon have a deeper look at this app.

So far we are not even sure it is a wallet ... Please check back later.