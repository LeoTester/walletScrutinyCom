---
title: "BuyUcoin - Popular Indian Crypto Currency Exchange"
altTitle: 

users: 5000
appId: com.buyucoinApp.buyucoin
launchDate: 
latestUpdate: 2020-11-12
apkVersionName: "3.0"
stars: 4.9
ratings: 219
reviews: 196
size: 10M
website: https://www.buyucoin.com/
repository: 
issue: 
icon: com.buyucoinApp.buyucoin.png
bugbounty: 
verdict: custodial # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-06-20
reviewStale: true
signer: 
reviewArchive:


providerTwitter: buyucoin
providerLinkedIn: company/buyucoin
providerFacebook: BuyUcoin
providerReddit: 

redirect_from:
  - /com.buyucoinApp.buyucoin/
  - /posts/com.buyucoinApp.buyucoin/
---


This app appears to be the broken interface for a broken exchange, judging by
the vast majority of user comments. It is certainly **not verifiable**.
