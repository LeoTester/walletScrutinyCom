---
title: "Crypto Exchange Currency.com"
altTitle: 

users: 50000
appId: com.currency.exchange.prod2
launchDate: 
latestUpdate: 2020-11-10
apkVersionName: "1.0.381"
stars: 4.4
ratings: 1203
reviews: 415
size: 17M
website: https://currency.com/
repository: 
issue: 
icon: com.currency.exchange.prod2.png
bugbounty: 
verdict: custodial # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-05-29
reviewStale: true
signer: 
reviewArchive:


providerTwitter: currencycom
providerLinkedIn: company/currencycom/
providerFacebook: currencycom/
providerReddit: 

redirect_from:
  - /com.currency.exchange.prod2/
  - /posts/com.currency.exchange.prod2/
---


This is an interface for a custodial trading platform and thus **not
verifiable**.