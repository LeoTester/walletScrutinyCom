---
title: "Bitnovo - Crypto Wallet"
altTitle: 

users: 10000
appId: com.bitnovo.app
launchDate: 
latestUpdate: 2020-09-24
apkVersionName: "2.7.1"
stars: 2.8
ratings: 207
reviews: 151
size: 13M
website: 
repository: 
issue: 
icon: com.bitnovo.app.png
bugbounty: 
verdict: wip # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-11-16
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.bitnovo.app/
---


This page was created by a script from the **appId** "com.bitnovo.app" and public
information found
[here](https://play.google.com/store/apps/details?id=com.bitnovo.app).

Probably an engineer will soon have a deeper look at this app.

So far we are not even sure it is a wallet ... Please check back later.