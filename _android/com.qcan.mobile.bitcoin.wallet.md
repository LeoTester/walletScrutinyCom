---
title: "Mobile Bitcoin Wallet"
altTitle: 

users: 10000
appId: com.qcan.mobile.bitcoin.wallet
launchDate: 
latestUpdate: 2020-11-11
apkVersionName: "0.8.848"
stars: 4.3
ratings: 96
reviews: 68
size: 28M
website: 
repository: 
issue: 
icon: com.qcan.mobile.bitcoin.wallet.png
bugbounty: 
verdict: wip # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-11-16
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.qcan.mobile.bitcoin.wallet/
---


This page was created by a script from the **appId** "com.qcan.mobile.bitcoin.wallet" and public
information found
[here](https://play.google.com/store/apps/details?id=com.qcan.mobile.bitcoin.wallet).

Probably an engineer will soon have a deeper look at this app.

So far we are not even sure it is a wallet ... Please check back later.