---
title: "Koinal: Buy Bitcoin instantly"
altTitle: 

users: 10000
appId: com.koinal.android
launchDate: 
latestUpdate: 2020-10-09
apkVersionName: "1.0.5"
stars: 4.8
ratings: 584
reviews: 310
size: 27M
website: 
repository: 
issue: 
icon: com.koinal.android.png
bugbounty: 
verdict: wip # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-11-16
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.koinal.android/
---


This page was created by a script from the **appId** "com.koinal.android" and public
information found
[here](https://play.google.com/store/apps/details?id=com.koinal.android).

Probably an engineer will soon have a deeper look at this app.

So far we are not even sure it is a wallet ... Please check back later.