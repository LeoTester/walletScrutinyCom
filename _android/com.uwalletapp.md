---
title: "Utrust Wallet"
altTitle: 

users: 5000
appId: com.uwalletapp
launchDate: 
latestUpdate: 2020-06-08
apkVersionName: "1.4.2"
stars: 4.3
ratings: 101
reviews: 56
size: 80M
website: 
repository: 
issue: 
icon: com.uwalletapp.png
bugbounty: 
verdict: wip # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-11-16
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.uwalletapp/
---


This page was created by a script from the **appId** "com.uwalletapp" and public
information found
[here](https://play.google.com/store/apps/details?id=com.uwalletapp).

Probably an engineer will soon have a deeper look at this app.

So far we are not even sure it is a wallet ... Please check back later.