---
title: "Decrypt - Bitcoin & crypto news"
altTitle: 

users: 10000
appId: co.decrypt.app
launchDate: 
latestUpdate: 2020-10-07
apkVersionName: "1.1.5"
stars: 4.6
ratings: 144
reviews: 58
size: 28M
website: 
repository: 
issue: 
icon: co.decrypt.app.png
bugbounty: 
verdict: wip # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-11-16
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /co.decrypt.app/
---


This page was created by a script from the **appId** "co.decrypt.app" and public
information found
[here](https://play.google.com/store/apps/details?id=co.decrypt.app).

Probably an engineer will soon have a deeper look at this app.

So far we are not even sure it is a wallet ... Please check back later.