---
title: "Buy Bitcoin, cryptocurrency - Spot BTC wallet"
altTitle: 

users: 50000
appId: com.spot.spot
launchDate: 
latestUpdate: 2020-11-12
apkVersionName: "4.13.1.2346-620e8696"
stars: 4.2
ratings: 3141
reviews: 1565
size: 63M
website: 
repository: 
issue: 
icon: com.spot.spot.png
bugbounty: 
verdict: wip # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-11-16
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.spot.spot/
---


This page was created by a script from the **appId** "com.spot.spot" and public
information found
[here](https://play.google.com/store/apps/details?id=com.spot.spot).

Probably an engineer will soon have a deeper look at this app.

So far we are not even sure it is a wallet ... Please check back later.