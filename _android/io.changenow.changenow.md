---
title: "ChangeNOW – Limitless Crypto Exchange"
altTitle: 

users: 10000
appId: io.changenow.changenow
launchDate: 
latestUpdate: 2020-10-25
apkVersionName: "1.99"
stars: 4.7
ratings: 369
reviews: 221
size: 5.7M
website: 
repository: 
issue: 
icon: io.changenow.changenow.png
bugbounty: 
verdict: wip # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-11-16
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /io.changenow.changenow/
---


This page was created by a script from the **appId** "io.changenow.changenow" and public
information found
[here](https://play.google.com/store/apps/details?id=io.changenow.changenow).

Probably an engineer will soon have a deeper look at this app.

So far we are not even sure it is a wallet ... Please check back later.