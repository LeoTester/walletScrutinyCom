---
title: "Celsius - Crypto Wallet"
altTitle: 

users: 100000
appId: network.celsius.wallet
launchDate: 
latestUpdate: 2020-10-19
apkVersionName: "4.6.0"
stars: 3.3
ratings: 1671
reviews: 1010
size: 91M
website: 
repository: 
issue: 
icon: network.celsius.wallet.png
bugbounty: 
verdict: wip # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-11-16
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /network.celsius.wallet/
---


This page was created by a script from the **appId** "network.celsius.wallet" and public
information found
[here](https://play.google.com/store/apps/details?id=network.celsius.wallet).

Probably an engineer will soon have a deeper look at this app.

So far we are not even sure it is a wallet ... Please check back later.