---
title: "OKEX - خرید بیت کوین"
altTitle: 

users: 10000
appId: co.okex.app
launchDate: 
latestUpdate: 2020-11-16
apkVersionName: "2.5.13"
stars: 3.9
ratings: 506
reviews: 223
size: 12M
website: 
repository: 
issue: 
icon: co.okex.app.png
bugbounty: 
verdict: wip # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-11-16
reviewStale: false
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /co.okex.app/
---


This page was created by a script from the **appId** "co.okex.app" and public
information found
[here](https://play.google.com/store/apps/details?id=co.okex.app).

Probably an engineer will soon have a deeper look at this app.

So far we are not even sure it is a wallet ... Please check back later.