---
title: "DigiFinex - Buy & Sell Bitcoin, Crypto Trading"
altTitle: 

users: 50000
appId: com.digifinex.app
launchDate: 
latestUpdate: 2020-11-16
apkVersionName: "2.3.20201116"
stars: 4.4
ratings: 2809
reviews: 2115
size: 63M
website: 
repository: 
issue: 
icon: com.digifinex.app.png
bugbounty: 
verdict: wip # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-11-16
reviewStale: false
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.digifinex.app/
---


This page was created by a script from the **appId** "com.digifinex.app" and public
information found
[here](https://play.google.com/store/apps/details?id=com.digifinex.app).

Probably an engineer will soon have a deeper look at this app.

So far we are not even sure it is a wallet ... Please check back later.